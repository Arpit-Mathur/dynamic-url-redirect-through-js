### What is this repository for? ###

This repository can be basically used in a scenario when the URL is not predefined on the click of a button , 
but actually it gets calculated once the button is clicked based on different conditions. So we use a JS approach 
to redirect the user to the URL in the New Tab.
This was used in the DOCUSIGN URL Creation but it is dynamic enough to be used for other use cases with similar conditions.