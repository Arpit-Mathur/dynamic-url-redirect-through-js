import { LightningElement,track } from 'lwc';
import getUrl from "@salesforce/apex/Redirect_Controller.getUrlAndCreateCase";
export default class LwcOpenLinkJS extends LightningElement {

    //Created Dummy List. This is fetched from Metadata in the project.
    @track qualfiyingEventsList= [
        {
            Event_Name__c : 'Test Event1'
        },
        {
            Event_Name__c : 'Test Event2'
        },
        {
            Event_Name__c : 'Test Event3'
        }
    ];
    selectedEvent;
    isDocuSign = false;
    openModal = false;
    eventURL;

    handleOpenModalJS(event){
        this.selectedEvent = this.qualfiyingEventsList[event.currentTarget.dataset.index];
       
        let eventTypes = ['Test Event1'];
        if(eventTypes.includes(this.selectedEvent.Event_Name__c)){
            // Does not calculate the URL here but will calculate the URL on click of Start Button.
            this.isDocuSign = true;
        }
        else{
            //It directly redirect to community page URL with selected event. Change this URL 
            this.eventURL = "/customerportal/s/qualifying-life-event?eventType="+encodeURIComponent(this.selectedEvent.Event_Name__c);
        }
        this.openModal = true;
    }

    handleStart(){
        if(this.isDocuSign){
            getUrl({qualifyingEventName : this.selectedEvent.Event_Name__c}).then(result => {
                if(result != null){
                // We get the Dynamic URL Created from Apex and now we will use this URL for opening it into new Tab
                 this.eventURL = result;
                 this.redirect_blank(this.eventURL);
                 this.openModal = false;
                 this.eventURL = null;
                }
               
                }).catch(error=>{
                     console.log('URL error>>',JSON.stringify(error));
                })
        }
    }
    //This function is used to open the Given URL in New Tab through JS.
    redirect_blank(url) {
        var a = document.createElement('a');
        a.target="_blank";
        a.href=url;
        a.click();
        a.href = '';
    }
}