public with sharing class Redirect_Controller {
   
     @AuraEnabled
    public static String getUrlAndCreateCase(String qualifyingEventName) {

        String url = '';
       //Creating the URL for the docusign purpose and that required dynamic caseId which is created after clicking on Start Now
        try{
            List<Case> casesToBeInserted = new List<Case>();
            id userId = UserInfo.getUserId();
            User u = [select id, contactId from User where id = : userId];
            id getContactId = u.contactId;
            List<Contact> loggedInUser = new List<Contact>();

            loggedInUser =
            [SELECT Id, Name,FirstName,LastName FROM contact where id =:getContactId];
            
            Case newCase;
            newCase = new Case(ContactId = loggedInUser[0].Id,
                Status = 'Draft'
                );

            // Create Case
            insert newCase;
            //Adding dynamic case id in url FOR DOCUSIGN purpose.
            url += '&EnvelopeField_SalesforceRecordID=' + newCase.Id;

            url += '&EnvelopeField_ContactId=' + loggedInUser[0].Id;

            if (loggedInUser[0].Email != null)
                url += '&UserEmail=' + loggedInUser[0].Email;

            if (loggedInUser[0].Phone != null)
                url += '&Phone=' + loggedInUser[0].Phone;

            casesToBeInserted.add(newCase);
            update  casesToBeInserted;
    
            return url;
        }
        catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
    
}
